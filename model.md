Jose Enrique Estremadoyro Fort A01018990
# Documento PDF

## Descripción del medio ambiente: 

El medio ambiente se compone de robots, muestras y naves 

### Cómo funciona 

### Qué permite hacer 
Permite escoger entre el comportamiento individual y el comportamiento cooperativo.

### Representación de los distintos de elementos que lo conforman 
Hay 2 elementos que lo conforman:
Las naves, donde se llevan las muestras y de donde surgen los personajes
Las muestras, que se recolectan y se llevan a la nave

### Representación de los agentes
Los agente son los robots
Tienen dos comportamientos:
individual y cooperativo
Estos se pueden cambiar al hacer click en un boton

## Descripción de la forma en que fueron implementadas las capas del modelo de Brooks.

## Posibles mejoras del programa.

## Conclusiones por cada integrante del equipo.
