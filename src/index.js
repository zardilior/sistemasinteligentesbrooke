import * as PIXI from 'pixi.js';
//import Robot from './robot.js';

// The application will create a renderer using WebGL, if possible,
// with a fallback to a canvas render. It will also setup the ticker
// and the root stage PIXI.Container

// The application will create a canvas element for you that you
// can then insert into the DOM

// Load world
const appOptions = {
    width:400,
    height:400
}
const tileSize = 50;
const loader = new PIXI.Loader();
loader.add([
    {"name":"world1","url":"resources/world.json"},
    {"name":"world2","url":"resources/world2.json"},
    {"name":"world3","url":"resources/world3.json"},
    {"name":"world4","url":"resources/world4.json"},
    {"name":"world5","url":"resources/world5.json"},
    {"name":"obstacle","url":"resources/obstacle.png"},
    {"name":"sample","url":"resources/sample.png"},
    {"name":"ship","url":"resources/ship.png"},
    {"name":"robot","url":"resources/robot.png"},
    {"name":"arrow","url":"resources/arrow.png"},

]).load(setup);

function setup(loader,resources){
    // Load images
    for(var i=1;i<=5;i++){
        let world = resources["world"+i].data;
        const app = new PIXI.Application(appOptions);
        app.objs = [];
        document.body.append(app.view);
        for(let obj of world){
            create(obj,resources,app) 
        }
        app.ticker.add(gameLoop(app))
    }
}
function play(app){
    this.prevx = this.x;
    this.prevy = this.y;
    switch(this.modules){
        case 5:
            var module5 = layer5.bind(this);
            module5(app);
        case 4:
            var module4 = layer4.bind(this);
            module4(app);
        case 3:
            var module3 = layer3.bind(this);
            module3(app);
        case 2:
            var module2 = layer2.bind(this);
            module2(app);
        case 1:
            var module1 = layer1.bind(this);
            module1(app);
    }
}
// avoid obstacles and boundaries, move in set direction
const minDistanceToObstacle = 50;
const speed = 3;
function layer1(app){
    for(let obstacle of app.objs.obstacle){
       const cond = Math.pow(minDistanceToObstacle,2) >= Math.pow(obstacle.x-this.x,2) +Math.pow(obstacle.y-this.y,2)
       if(cond){
           // get the perpendicular line to the object and follow it
           let ySign = Math.sign(obstacle.y-this.y);
           if(ySign==1)
               this.degree = Math.atan((obstacle.x -this.x) / (obstacle.y-this.y));
           else
               this.degree = Math.PI + Math.atan((obstacle.x -this.x) / (obstacle.y-this.y));
       }
    }
    this.x += speed*Math.cos(this.degree);
    this.y += speed*Math.sin(this.degree);
    //console.log("x",speed*Math.cos(this.degree));
    //console.log("y",speed*Math.sin(this.degree));
    if(this.x < 0){
        this.x = 0;
        this.degree = Math.PI/2;
    }
    if(this.y < 0){
        this.y = 0;
        this.degree = Math.PI
    }
    if(this.x > 350){
        this.x = 350;
        this.degree = Math.PI*3/2;
    }
    if(this.y > 350){
        this.y = 350;
        this.degree = 0;
    }
}
// if no food found, assign a random direction every x moments, to wander off
const stuckParam = speed/2;
const randSpeed = speed;
const counterInterval = 40;
let layer2 = wanderOff();
function wanderOff(){
    let counter=0;
    return function(app){
        if(!this.foodLocation && !this.arrow){
            if(counter++==counterInterval){
                this.degree += 2*Math.PI * Math.random();
                counter = 0;
            }
        }
    }
}
// find food, and return to the food
const aConstant = 10;
const minDistanceToObject = 50;
const minDistanceToFoodLocation = 30;
function layer3(app){
    // if food return to ship
    // else see obstacles
    if(!this.hasFood){
        if(this.foodLocation){
           const cond = Math.pow(minDistanceToFoodLocation,2) >= Math.pow(this.foodLocation.x-this.x,2) +Math.pow(this.foodLocation.y-this.y,2)
           if(!cond){
               let xSign = Math.sign(this.foodLocation.x-this.x);
               if(xSign==1)
                   this.degree = Math.atan((this.foodLocation.y -this.y) / (this.foodLocation.x-this.x));
               else
                   this.degree = Math.PI + Math.atan((this.foodLocation.y -this.y) / (this.foodLocation.x-this.x));
                // if on ship leave food
           }
            else{
                this.foodLocation = null;
            }
        }
        for(let i in app.objs.sample){
           let sample = app.objs.sample[i];
           const cond = Math.pow(minDistanceToObject,2) >= Math.pow(sample.x-this.x,2) +Math.pow(sample.y-this.y,2)
           if(cond){
               this.foodLocation = {x:this.x,y:this.y};
               this.hasFood = true;
               this.arrow = false;
               app.stage.removeChild(sample);
               app.objs.sample.splice(i,1);
               this.degree -= Math.PI;
               break;
               break;
           }
        }
    }
    else{
       // return to the ship
       let ship = app.objs.ship[0];
       // get the perpendicular line to the object and follow it
       const cond = Math.pow(minDistanceToObject,2) >= Math.pow(ship.x-this.x,2) +Math.pow(ship.y-this.y,2)
       if(!cond){
           let xSign = Math.sign(ship.x-this.x);
           if(xSign==1)
               this.degree = Math.atan((ship.y -this.y) / (ship.x-this.x));
           else
               this.degree = Math.PI + Math.atan((ship.y -this.y) / (ship.x-this.x));
            // if on ship leave food
       }
       else{
           this.hasFood = false;
           this.arrow = false;
           this.degree -= Math.PI;
       }

    }
}
// leave marks with the direction if food found
let layer4 = leaveMarks();
function leaveMarks(){
    const counterInterval = 20;
    const dissapearInterval = 1000;
    let counter = 0;
    return function(app){
        //leave marks in the direction of the food if it has food
        if(this.foodLocation && counter++>counterInterval){
            counter = 0;
            let arrow = new PIXI.Sprite(app["arrow"]);
            if(app.objs["arrow"]){
                app.objs["arrow"].push(arrow);
            }
            else{
                app.objs["arrow"] = [arrow];
            }
            let xSign = Math.sign(this.foodLocation.x-this.x);
            if(xSign==1)
                arrow.degree = Math.atan((this.foodLocation.y -this.y) / (this.foodLocation.x-this.x));
            else
                arrow.degree = Math.PI + Math.atan((this.foodLocation.y -this.y) / (this.foodLocation.x-this.x));
            let n = app.objs.length;
            arrow.x = this.x;
            arrow.y = this.y;
            arrow.width = tileSize;
            arrow.height = tileSize;
            arrow.rotation = arrow.degree;
            app.stage.addChild(arrow);
            setTimeout(()=>{
                app.objs.arrow.splice(n-1,1);
                app.stage.removeChild(arrow); 
            },dissapearInterval)
        }
    }
}
// detect marks and follow them
const minDistanceToSeeArrow = 100;
const minDistanceToFollowArrow = 50;
function layer5(app){
    if( app.objs.arrow && !this.foodLocation){
        for(let arrow of app.objs.arrow){
           const condFollow = Math.pow(minDistanceToFollowArrow,2) >= Math.pow(arrow.x-this.x,2) +Math.pow(arrow.y-this.y,2)
           const condSee = Math.pow(minDistanceToSeeArrow,2) >= Math.pow(arrow.x-this.x,2) +Math.pow(arrow.y-this.y,2)
           if(condFollow){
               let xSign = Math.sign(arrow.x-this.x);
               if(xSign==1)
                   this.degree = Math.atan((arrow.y -this.y) / (arrow.x-this.x)) || this.degree;
               else
                   this.degree = Math.PI + Math.atan((arrow.y -this.y) / (arrow.x-this.x)) || this.degree;

           }
           else if(condSee && !this.arrow){
               this.degree = arrow.degree;
               this.arrow = true;
           }
        }
    }
}
function create(obj,resources,app){
    let sprite = new PIXI.Sprite(resources[obj.type].texture);
    app.arrow = resources.arrow.texture;
    if(obj.type=="robot"){
        sprite.degree = 0;
        sprite.play = play.bind(sprite);
        sprite.modules = obj.modules;
    }
    if(app.objs[obj.type]){
        app.objs[obj.type].push(sprite);
    }
    else{
        app.objs[obj.type] = [sprite];
    }
    sprite.x = obj.x;
    sprite.y = obj.y;
    sprite.width = tileSize;
    sprite.height = tileSize;
    app.stage.addChild(sprite);
}
function gameLoop(app){
    return function(delta){
        // here the app exists 
        // so execute the robots play func
        for(let robot of app.objs.robot){
            robot.play(app);
        }
    }
}
