Jose Enrique Estremadoyro Fort A01018990
# Documento PDF Entrehable

## Descripción del medio ambiente: 
Son 5 medios ambientes, cada uno aplica un layer adicional.
Estos ambientes se conforman de:
Obstaculos, un asteroide
Robots, un arduino circular
Muestras, circulos que parecen un planeta tierra
Nave, claramente una nave

En cada ambiente se prueba el comportamiento de un layer de izquierda a derecha, de arriba a abajo

Las descripciones de estos medios ambientes se encuentran en:
public/resources/world.json
public/resources/world2.json
public/resources/world3.json
public/resources/world4.json
public/resources/world5.json

### Cómo funciona 
Para correrlo se debe tener instalado npm y correr en el root del proyecto:
```
npm run buildAndRun
```
Despues de esto se accede a la liga provista, usualmente localhost:3000 para verlo en accion
Para reiniciarlo se refreshea el browser.

### Qué permite hacer 
Permite ver todas las capas en diferentes escenarios hechos para probarlas

### Representación de los distintos de elementos que lo conforman 
Hay 2 elementos que lo conforman:
Obstaculos, un asteroide, muestran los bloqueos
Robots, un arduino circular, muestran nuestro robot automata
Muestras, circulos que parecen un planeta tierra, muestran las muestras a regresar a la nave
Nave, claramente una nave, muestran el lugar a donde se tiene que regresar la comida

### Representación de los agentes

## Descripción de la forma en que fueron implementadas las capas del modelo de Brooks.
Capa 1: No chocar ni salirse, al encontrar un obstaculo o el borde se mueven en una direccion perpendicular a la linea que conecta ambos puntos.
Capa 2: Si no se tiene un punto de comida encontrado, cambiar de direccion aleatoriamente cada x segundos
Capa 3: Maneja la logica de encontrar la comida, ir a la nave, regresar al punto recordado para buscar mas y olvidarlo si no se encuentra mas
Capa 4: Maneja la logica de dejar flechas apuntando cada x segs a la comida si se tiene recuerdo de este lugar
Capa 5: Maneja la logica de acercarse a las flechas y seguir su direccion

La forma en que se implementaron las capas

## Posibles mejoras del programa.
- Agregar una capa para que no se desatoren.
- Mejorar la capa de evadir obstaculos, porque el moverse perpendicular a ellos se vaya a chocar o no, no funciona correctamente
- Agregar la mapa y otros robots como obstaculos

## Conclusiones por cada integrante del equipo.
Jose Enrique Estremadoyro Fort
En conclusion las leyes de brook no son complicadas o laboriosas, lo laborioso fue implementar el ambiente y la base programatica y lo complicado y laborioso fue probar con diferentes implementaciones que cumplieran las capas. El tener 5 ambientes 1 por cada layer adicional facilito las pruebas. Y en un punto se tuvieron que borrar capa 2 y 3 para modificar la capa 1, mas no fue tan complicado.
