# instrucciones.md

## Instrucciones

1. Implementar un medio ambiente en el que pueda ser adaptado el “explorador de marte”.

2. Implementar el “explorador de marte” en el medio ambiente del punto 1 de las instrucciones. Se deben de implementar:

  	a. Solución con comportamiento individual (no cooperativo).

  	b. Solución con comportamiento 	colaborativo.

## Rúbrica de evaluación

1. Programa del “explorador de marte”:

	a. (30 puntos) Código fuente

	b. (10 puntos) Código ejecutable

	c. (10 puntos) Breve manual de usuario – el objetivo es poder usar el programa fácilmente, se espera que la GUI se intuitiv	y que solamente se documente en el manu	al 	cualquier detalle fuera de lo normal para la 	correcta ejecución del programa.

2. Documento PDF que contenga:

	a. (10 puntos) Descripción del medio ambiente: cómo funciona, qué permite hacer, representación de los distintos de elementos que lo conforman, representación de los agentes, et	c.

	b. (10 puntos) Descripción de la forma en que fueron implementadas las capas del modelo de Brooks.

	c. (5 puntos) Posibles mejoras del programa.

	d. (5 puntos) Conclusiones por cada integrante del equipo.

3. Revisión frente al grupo:

	a. (20 puntos) En la fecha de entrega se hará una revisión aleatoria a 3 equipos con la intención de corroborar la manera en que fueron implementadas las capas del modelo de Brooks. Se pedirá que muevan de lugar las capas para comprobar la correcta implementación del modelo.

## Entregables 

Enviar un correo electrónico a mgonza@tec.mx con el asunto “Actividad 1 Programacion SI” que contenga:

1. Archivo .zip conteniendo el programa.

2. Documento PDF con la descripción solicitada.

3. Copia a todos los miembros del equipo (- 10 puntos en caso de omisión)

4. Video de 2 minutos “presumiendo” su programa (liga en YouTube).
