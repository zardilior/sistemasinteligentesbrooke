# Actividad de Programación 1 – Arquitectura Reactiva

En el enfoque de la inteligencia artificial actual, buscamos que lo programado para investigar la inteligencia humana sea útil en el mundo real o “situado en un medio ambiente”. Este requisito es importante para poder probar que lo desarrollado en un programa puede “convivir” con otros programas en un medio ambiente. Por lo tanto, es importante la selección del medio ambiente en el que se desarrollará un programa de IA.

Para esta actividad deberás de implementar el medio ambiente del “explorador de marte” visto en clase. Si te es posible, puedes utilizar un medio ambiente que ya este desarrollado y adaptarlo a las necesidades del medio ambiente del explorador de marte.

Las instrucciones del profesor se encuentran en [instrucciones](instrucciones.md)
El PDF entregable del profesor se encuentran en [entregable](entregable.md)

